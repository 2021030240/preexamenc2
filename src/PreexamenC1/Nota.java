/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PreexamenC1;

/**
 *
 * @author Administrator
 */
public abstract class Nota {
    protected int numeroN;
    protected String fecha;
    protected String concepto;
    protected Parecederos productoP;
    protected int cantidad; 
    protected int tipoPago;

    public Nota() {
        this.numeroN = 0;
        this.fecha = "";
        this.concepto="";
        this.productoP = new Parecederos();
        this.cantidad = 0;
        this.tipoPago = 0;
    }

    public Nota(int numeroN, String fecha, String concepto, Parecederos productoP, int cantidad, int tipoPago) {
        this.numeroN = numeroN;
        this.fecha = fecha;
        this.concepto = concepto;
        this.productoP = productoP;
        this.cantidad = cantidad;
        this.tipoPago = tipoPago;
    }

    public int getNumeroN() {
        return numeroN;
    }

    public void setNumeroN(int numeroN) {
        this.numeroN = numeroN;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Parecederos getProductoP() {
        return productoP;
    }

    public void setProductoP(Parecederos productoP) {
        this.productoP = productoP;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(int tipoPago) {
        this.tipoPago = tipoPago;
    }
    
    public abstract float calcularPago();  
}
