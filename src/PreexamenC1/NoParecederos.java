/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PreexamenC1;

/**
 *
 * @author Administrator
 */
public class NoParecederos extends Productos{
    private int loteF;

    public NoParecederos() {
        this.loteF=0;
    }

    public NoParecederos(int loteF, int idProducto, String nombreP, float unidadPro, float precioUni) {
        super(idProducto, nombreP, unidadPro, precioUni);
        this.loteF = loteF;
    }
    
    public int getLoteF() {
        return loteF;
    }

    public void setLoteF(int loteF) {
        this.loteF = loteF;
    }
    
   public float calcularPrecioNoUni(){
       return this.precioUni * 0.50f + this.precioUni;
   }
        
    
}
