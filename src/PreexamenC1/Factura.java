/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PreexamenC1;

/**
 *@author Administrator
 */
public class Factura extends Nota implements Impuesto{

    protected int numRFC;
    protected String nombre;
    protected String domicilio;

    public Factura() {
        this.numRFC=0;
        this.nombre="";
        this.domicilio="";
    }

    public Factura(int numRFC, String nombre, String domicilio, int numeroN, String fecha, String concepto, Parecederos productoP, int cantidad, int tipoPago) {
        super(numeroN, fecha, concepto, productoP, cantidad, tipoPago);
        this.numRFC = numRFC;
        this.nombre = nombre;
        this.domicilio = domicilio;
    }

    public int getNumRFC() {
        return numRFC;
    }

    public void setNumRFC(int numRFC) {
        this.numRFC = numRFC;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    @Override
    public float calcularPago() {
        return this.cantidad * this.productoP.calcularPrecioUni(this.productoP.unidadPro);
    }

    @Override
    public float calcularImpuesto() {
       return this.calcularPago()*0.16f;
    }
    
}
