/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PreexamenC1;

/**
 *
 * @author Administrator
 */
public class Parecederos extends Productos{
    private String fechaCaducidad;
    private float temperatura;

    public Parecederos() {
        this.fechaCaducidad="";
        this.temperatura=0.0f;
    }

    public Parecederos(String fechaCaducidad, float temperatura, int idProducto, String nombreP, float unidadPro, float precioUni) {
        super(idProducto, nombreP, unidadPro, precioUni);
        this.fechaCaducidad = fechaCaducidad;
        this.temperatura = temperatura;
    }

    public String getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(String fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }
    
    
    public float calcularPrecioUni(float unidad){
        float total=0.0f;
        if (unidad==1)
           total=this.precioUni + (this.precioUni*0.03f);
        
        else if(unidad==2)
           total=this.precioUni + (this.precioUni*0.05f);
        
        else
            total=this.precioUni + (this.precioUni* 0.04f);
        
        total = total + (total*0.50f);
        return total;
    
    }   
    
}
