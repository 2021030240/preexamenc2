/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PreexamenC1;

/**
 *
 * @author Administrator
 */
public abstract class Productos {
    protected int idProducto;
    protected String nombreP;
    protected float unidadPro;
    protected float precioUni;

    public Productos() {
        
        this.idProducto = 0;
        this.nombreP = "";
        this.unidadPro = 0.0f;
        this.precioUni = 0.0f;
        
    }

    public Productos(int idProducto, String nombreP, float unidadPro, float precioUni) {
        this.idProducto = idProducto;
        this.nombreP = nombreP;
        this.unidadPro = unidadPro;
        this.precioUni = precioUni;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreP() {
        return nombreP;
    }

    public void setNombreP(String nombreP) {
        this.nombreP = nombreP;
    }

    public float getUnidadPro() {
        return unidadPro;
    }

    public void setUnidadPro(float unidadPro) {
        this.unidadPro = unidadPro;
    }

    public float getPrecioUni() {
        return precioUni;
    }

    public void setPrecioUni(float precioUni) {
        this.precioUni = precioUni;
    }
}
